FROM openjdk:8-jdk-alpine
WORKDIR /usr/src/app
COPY build/libs/*.jar app.jar
ENTRYPOINT ["java","-jar","/usr/src/app/app.jar"]